FROM microsoft/mssql-server-windows-developer:latest

RUN start-service MSSQLSERVER

RUN sqlcmd.exe -Q "\"ALTER LOGIN sa with password='8Octet*88'; ALTER LOGIN sa ENABLE;\""

WORKDIR c:/

RUN mkdir SQLData

COPY ./backup/db_latest.bak ./backup/db_latest.bak

RUN sqlcmd.exe -S . -U sa -P '8Octet*88' -Q "\"RESTORE DATABASE [AdventureWorks] FROM DISK='C:\\backup\\db_latest.bak' WITH MOVE 'AdventureWorks2017' TO 'c:\\SQLData\\AdventureWorks.mdf', MOVE 'AdventureWorks2017_Log' TO 'c:\\SQLData\\AdventureWorks.ldf'\""

RUN powershell.exe -Command Remove-Item c:\backup\db_latest.bak -Force

CMD [ "cmd" ]