import Mustache from 'mustache';
import * as fs from 'fs';

const environment = ((process.argv && process.argv[2]) || '').toLowerCase();
let configFile;
switch (environment) {
    case 'qa1':
    case 'qa2':
    case 'dev':
        configFile = './configs/compose-config.json';
        break;
    default:
        throw Error(`Not supported or missing environment: ${environment}`)
}

const template = fs.readFileSync('./docker-compose-template.yaml', "utf8");
const jsonConfig = JSON.parse(fs.readFileSync(configFile, "utf8"));

const output = Mustache.render(template, jsonConfig[environment]);

fs.writeFileSync('./docker-compose.yaml', output);